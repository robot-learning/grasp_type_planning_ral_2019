### What is this repository for? ###
This is the grasp learning and inference code for the RA-L 2019 grasping paper "Modeling Grasp Type Improves Learning-Based Grasp Planning". 
We implemented a probabilistic grasp planner that explicitly models grasp type for planning high-quality precision and power grasps in real-time. 

### Learning and Inference ###
Please refer to `grasp_pgm_learner.py` for the grasp model learning and `grasp_pgm_inference.py` for grasp inference. 

#### Learning:
Key learning functions:      
`train_all()` trains and saves logistic regression model using all training grasps. 
`fit_prior()` fits and saves the GMM prior model of the grasp configuration. 

#### Inference:    
Key inference functions:
`quasi_newton_lbfgs_inf()` infers the grasp with max posterior using LBFGS. 

### Grasp Planning ###

Command to launch the grasp planner: ``roslaunch grasp_type_inference grasp_pgm_inf.launch``.

The grasp inference server "grasp_pgm_inference" of the file "grasp_pgm_inf_server.py" takes the grasping scene point cloud, 
the grasp configuration initialization and the grasp type as inputs and generates the optimized grasp as the output. 
The grasp inference server calls `quasi_newton_lbfgs_inf()` of `grasp_pgm_inference.py`. 
